# Chroniques Oubliées Fantasy - Item Effects #  
  
Ce module ajoute un onglet sur les objets pour y ajouter des effets.  
Dès que l'objet est placé dans un inventaire, l'effet est appliqué au personnage.  
ex : Ceinture de force de géant modifie automatiquement la force du personnage.  
  
Fonctionne également pour les capacités et permet d'ajouter des effets au personnage lorsque le joueur choisi ses capacités.  
ex : Dès que le joueur choisi la capacité Vivacite pour son guerrier, le personnage voit son initiative être mise à jour automatiquement sans aucune intervention.  