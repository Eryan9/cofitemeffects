import {CustomCofItemSheet} from "./items/item-sheet.js";

Hooks.once("ready", async function(){
    let itemSheet = Items.registeredSheets.find(sheet=>sheet.name === "CofItemSheet");

    if (itemSheet){
        Items.unregisterSheet("cof", itemSheet);

        // Register item sheets
        Items.registerSheet("customCof", CustomCofItemSheet, {
            types: ["item", "capacity", "profile", "path", "species"],
            makeDefault: true,
            label: "ItemSheet"
        });        
    }

    preloadHandlebarsTemplates();
})

async function preloadHandlebarsTemplates() {
    loadTemplates(
        [
            "modules/cofItemEffects/templates/item-sheet.hbs",
            "modules/cofItemEffects/templates/actor-effects.hbs",
            "modules/cofItemEffects/templates/actor-effects-item.hbs"
        ]
    );
}