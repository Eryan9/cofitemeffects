import {CofItemSheet} from "/systems/cof/module/items/item-sheet.js";

export class CustomCofItemSheet extends CofItemSheet{
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["cof", "sheet", "item", this.type],
            template: `/modules/cofItemEffects/templates/item-sheet.hbs`,
            width: 600,
            height: 600,
            tabs: [{navSelector: ".sheet-navigation", contentSelector: ".sheet-body", initial: "description"}],
            dragDrop: [{dragSelector: ".item-list .item", dropSelector: null}]
        });
    }

    activateListeners(html) {
        super.activateListeners(html);  

        html.find('.effectName').click(ev=>{
            ev.preventDefault();
            const elt = $(ev.currentTarget).parents(".effect");
            if (!elt) this._onEditItem.bind(this)
            else {
                const effectId = elt.data("itemId");
                let effect = this.item.effects.get(effectId);
                if (effect){
                    new ActiveEffectConfig(effect).render(true);
                }
            }
        });
        html.find('.effect-edit').click(ev => {
            ev.preventDefault();
            const elt = $(ev.currentTarget).parents(".effect");
            const effectId = elt.data("itemId");
            let effect = this.item.effects.get(effectId);
            if (effect){
                new ActiveEffectConfig(effect).render(true);
            }
        });        
        html.find('.effect-create').click(ev => {
            ev.preventDefault();
            return ActiveEffect.create({
                label: game.i18n.localize("COF.ui.newEffect"),
                icon: "icons/svg/aura.svg",
                "duration.rounds": undefined,
                disabled: false
            }, this.item).create();
        });
        html.find('.effect-delete').click(ev => {
            ev.preventDefault();
            const elt = $(ev.currentTarget).parents(".effect");
            const effectId = elt.data("itemId");
            let effect = this.item.effects.get(effectId);
            if (effect) effect.delete();
        });
        html.find('.effect-toggle').click(ev => {
            ev.preventDefault();
            const elt = $(ev.currentTarget).parents(".effect");
            const effectId = elt.data("itemId");
            let effect = this.item.effects.get(effectId);
            if (effect){
                effect.update({disabled:!effect.data.disabled})
            }
        });          
    } 

    getData(options) {
        let data = super.getData(options);
        
        data.effects = data.item.effects;

        return data;
    }    
}